import * as path from 'path';
import marked from 'marked';
import * as fs from 'fs';
import {CONTENT_DIRECTORY, BLOG_POSTS_DIRECTORY, OUTPUT_DIRECTORY} from './config.js';
import {loadManyBlogPostSourceFiles} from './blog-post/load-blog-post-source-file.js';
import {loadManyBlogPosts} from './blog-post/load-blog-post.js';
import {sortByDateCreatedDescending} from './blog-post/sort-blog-post.js';
import {renderTemplate} from './_deps/render-template.js';

const navigationItems = [
  {path: 'https://jskatas.org', name: 'jskatas.org'},
  {path: '/', name: 'Blog'},
  {path: '/about', name: 'About'},
];
const defaultRenderParams = {
  navigationItems,
  site: {
    title: 'JSKatas Blog',
    subtitle: 'Continuously Learn JavaScript. Your Way.',
    domain: 'blog.jskatas.org',
  },
};

const generate301Page = async (oldPath, newPath) => {
  const destDir = path.join(OUTPUT_DIRECTORY, oldPath);
  await fs.promises.mkdir(destDir, {recursive: true});
  const destFilename = path.join(destDir, 'index.html');
  const renderedFile = renderTemplate('301.html', {...defaultRenderParams, redirectUrl: newPath});
  await fs.promises.writeFile(destFilename, renderedFile);
  // console.log("Built 301 page ", destFilename);
}

const generate301Pages = (post) => {
  if (post.oldUrls.length > 0) {
    return Promise.all(post.oldUrls.map(oldUrl => generate301Page(oldUrl, post.url)));
  }
}

const generate404Page = async (posts) => {
  const destDir = OUTPUT_DIRECTORY;
  await fs.promises.mkdir(destDir, {recursive: true});
  const destFilename = path.join(destDir, '404.html');
  const renderedFile = renderTemplate('404.html', {...defaultRenderParams, posts});
  await fs.promises.writeFile(destFilename, renderedFile);
}

const generatePost = async (post) => {
  const destDir = path.join(OUTPUT_DIRECTORY, post.url);
  await fs.promises.mkdir(destDir, {recursive: true});
  const destFilename = path.join(destDir, 'index.html');
  const renderedFile = renderTemplate('blog/post.html', {...defaultRenderParams, post});
  await fs.promises.writeFile(destFilename, renderedFile);
}

const aboutIndexPage = async () => {
  const destDir = path.join(OUTPUT_DIRECTORY, 'about');
  await fs.promises.mkdir(destDir, {recursive: true});
  const destFilename = path.join(destDir, 'index.html');
  const content = marked(await fs.promises.readFile(path.join(CONTENT_DIRECTORY, 'about/index.md'), 'utf8'));
  const renderedFile = renderTemplate('about/index.html', {...defaultRenderParams, content});
  await fs.promises.writeFile(destFilename, renderedFile);
};
const generateAboutPages = async () => {
  await aboutIndexPage();
}


const generateBlogOverviewPage = async (posts) => {
  const renderedFile = renderTemplate('blog/index.html', {...defaultRenderParams, posts});
  const destFilename1 = path.join(OUTPUT_DIRECTORY, 'blog/index.html');
  await fs.promises.writeFile(destFilename1, renderedFile);
};

const generateHomePage = async (posts) => {
  const renderedFile = renderTemplate('home.html', {...defaultRenderParams, posts});
  const destFilename = path.join(OUTPUT_DIRECTORY, 'index.html');
  await fs.promises.writeFile(destFilename, renderedFile);
};

const runAndTimeIt = async (label, fn) => {
  const paddedLabel = (label + new Array(20).fill(' ').join('')).substr(0, 25);
  console.time(paddedLabel);
  try {
    await fn();
  } catch(e) {
    console.error(e);
    process.exit(1); 
  }
  console.timeEnd(paddedLabel);
}

const isNotDraft = post => post.isDraft === false;
const loadPosts = async sourceFiles => {
  const posts = (await loadManyBlogPosts()(sourceFiles)).sort(sortByDateCreatedDescending);
  posts.excludingDrafts = () => posts.filter(isNotDraft);
  return posts;
}

(async() => {
  console.time('Overall');
  console.log('Preparing data\n========');
  console.time('Load source files');
  const sourceFiles = await loadManyBlogPostSourceFiles()(BLOG_POSTS_DIRECTORY);
  console.timeEnd('Load source files');
  console.time('Load blog posts');
  const posts = await loadPosts(sourceFiles);
  console.timeEnd('Load blog posts');

  console.log('\nBuilding pages\n========');
  await runAndTimeIt('Home page', () => generateHomePage(posts.excludingDrafts()));
  // blog
  console.log('Blog');
  await runAndTimeIt(`  all posts (${posts.length})`, () => Promise.all(posts.map(generatePost)));
  // await runAndTimeIt('  /blog page', () => generateBlogOverviewPage(posts.excludingDrafts()));

  await runAndTimeIt('About pages', () => generateAboutPages());

  console.log('HTTP pages');
  const oldUrlsCount = posts.reduce((prev, cur) => cur.oldUrls.length + prev, 0);
  await runAndTimeIt(`  301 pages (${oldUrlsCount})`, () => Promise.all(posts.map(generate301Pages)));
  await runAndTimeIt('  404 page', () => generate404Page(posts.slice(0, 5)));
  
  console.log('-----');
  console.timeEnd('Overall');
  console.log('-----');
})();

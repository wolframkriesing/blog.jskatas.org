dateCreated: 2021-01-09 22:47 CET
tags: jskatas, mission, vision, explained
previewImage: preview.jpg

# JSKatas - Explained

This article explains the vision of jskatas.org.  
In short it is:  
**Continuously Learn JavaScript. Your Way.**

It used to be:   
**Learn and re-learn (all of) JavaScript, at your level and at your speed.**  
The above is shorter, more explicit and is more visionary. I might write about the way to getting there at some point.


No matter if you are beginner or an expert, a slow or fast learner.
The site jskatas aims to allow anyone to discover and learn unknown or new parts of JavaScript.
You can also consolidate your JavaScript knowledge or extend
your skills and sharpen them.

Learning with jskatas requires you to **actively** work with and on the language. There
is no lean-back content to "just consume" and hope to have acquired knowledge,
this is NOT what jskatas offers, and also does not want to offer.

## Content

1. [Learn (With Tests)](#learn-with-tests)
1. [Re-Learn](#re-learn)
1. [(All of) JavaScript](#all-of-javascript)
1. [At Your Level and At Your Speed](#at-your-level-and-at-your-speed)

## Learn (With Tests)

JSKatas strives to allows you to learn the language JavaScript profoundly and complete.
The approach to learn is oriented around tests.

The katas are structured closely with how the language evolves.
Categories like "ECMAScript 6 (ES2015)" which had been introduced
provide a versioning scheme for the language.
JSKatas structures its learnings using those categories and will allow you to track
and learn all parts of JavaScript.

The learning itself can be called test-driven, because all one does is working with tests all the time.
Tests are an essential part of programming. How can you continuously prove that code works if
there are not coherent and expressive tests that can verify the functioning at any time?
Why not apply this approach for learning a programming language? That was the thought
when jskatas was created.

A test is made up of a good test description explicitly describing what the test is proving
or trying to prove. Each test description you find on jskatas strives to be explaining what
the code (the test) is teaching. Each test on jskatas represents a tiny bit of knowledge.
For example: `'isInteger' is a static function on 'Number'`. This piece of info is knowledge
you can take away as soon as you read the test description. To make this knowledge really
stick the according test is failing and the learner is supposed to solve it. Sometimes solving
is a number of tries and thought processes, or call them learnings. Once learned by having
read and proven by making the initially failing test pass, the knowledge might stick easier and
once needed will be easy to recall and apply.

## Re-Learn

Re-learn is the part that I am most excited about and that I find most useful about jskatas
and, I believe that this part is far underevaluated by many.
Actually I am not really happy with the term "re-learn", I am not sure if it captures the
intent best. Let's explore what it means.

I use jskatas to re-learn parts of JavaScript that I either did not use for a long time,
that I never used, that I forgot or parts where I am not sure how they really work.
Often I had come back to do the Map-katas because I used `Map` sporadically.   
Another one that I often get wrong, even after many years of JavaScript is the Array method `sort()`.
So I wrote the 
[ES1 kata](https://jskatas.org/#bundle-es1-katas) for `sort()`. I came back to it
at least twice already again to re-learn how `sort()` works. The last time was just last week.
The `sort()` function is in JavaScript since ES1, for a long time already, 
but it's not that intuitive that I get it right all the time. It might just be me.

JSKatas allows you to jump in whereever you want and either re-learn a part of JavaScript
or harden your existing knowledge. There is no entry or exit point on jskatas, any point
can be a starting and ending point for learning. There are no fixed lessons or plans you
have to follow when learning. There are only pieces of knowledge that can be arranged and
re-arranged any time depending on the current interest.
That's why jskatas is also ideal for re-learning and hardening ones JavaScript knowledge.

In the future test plans and guides might be provided, to give guidance on how to
learn, what to learn next, where to start and/or where to continue. 
But this is still some time out. Even given these guides would exist, learning can still
start, continue and end anywhere with any kata. Such a guide is easy to arrange depending on the
desired learning goal. And yet it can make use of the entire pool of katas.

## (All of) JavaScript

As mentioned before JavaScript is very well structured, versioned and named, in it's own way though.
"ECMAScript 1 (ES1)", "ES5", "ES6" or "ES2015", the names make the evolution of JavaScript easy to understand
and allow jskatas to easily structure all the knowledge.

Due to this crystal clear and simple structure of the JavaScript knowledge it is also very easy to
measure how much of JavaScript one can learn using jskatas, and also how much jskatas provides
to be learned. Currently I would claim that [80% of ES6](https://jskatas.org/#bundle-es6-katas) 
can be learned using jskatas. But how much of all JavaScript, that is left to be made measurable correctly.
This is not done yet.

That's why the "All of" in the headline of this paragraph is set in parantheses. 
JSKatas does not allow to learn all of JavaScript yet.
But measuring it is not that difficult using the approach of providing the knowledge as jskatas does it.

## At Your Level and At Your Speed

The level of knowledge and the speed of learning is so very different among people, that
everyone has their own way of learning.
One person knows all about arrays, but not all about string, the next one knows half of each.
Offering these people the same learning materials is not efficient.

On jskatas you determine the speed at which you solve the katas that provide the knowledge.
Let's see an example. The Array API katas for ES6 consist of eight katas.

<figure>
    <img src="es6-array-katas.png" alt="The eight ES6 Array katas." width="200" />
    <figcaption>The eight ES6 Array katas.</figcaption>
</figure>

The `Array.from()` kata consists of five tests, or five pieces of knowledge.
You can always decide how much of the kata you do, if you use it for re-learning
or if you want to learn all the kata provides for this topic.

Every tiny test is a progress. 
One test can take as little as five seconds to solve.
So even re-learning might not be a huge time invest.
And you can stop learning even within a kata whenever you like.

So the depth and intensity is up to you, your discipline, interest, drive and
endurance. JSKatas allows you to learn at your level and your speed.
